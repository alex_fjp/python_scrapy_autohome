# python_scrapy_autohome

#### 介绍
年后想学习下python爬虫，刚好准备买车，于是花了一周的时间用scrapy写的一个爬虫，可以爬取汽车之家车辆的口碑评分，
然后插入到MongoDB里。

这次拉取了雅阁、凯美瑞、crv、rav、cx-5共5台车2w多条评论。

![雷达图](https://images.gitee.com/uploads/images/2019/0228/184513_322d9226_1554742.jpeg "code_test_result.jpg")

![image](https://images.gitee.com/uploads/images/2019/0228/184642_2df5bb74_1554742.png)

![image](https://images.gitee.com/uploads/images/2019/0228/184643_c47343de_1554742.png)

#### 软件架构

1. Python
2. scrapy
3. MongoDB
4. pyecharts 

#### 安装教程

1. 安装爬虫框架：scrapy。
2. 安装mongodb.
3. 在autohome.settings.py配置里修改mongodb的连接地址。
4. 在autohome.autohome_spider.py的start_requests(self)方法中配置需要爬的车辆代码（车辆代码可在汽车之家网站的路径里找到）。
![image](https://images.gitee.com/uploads/images/2019/0228/184642_07d98819_1554742.png)

![image](https://images.gitee.com/uploads/images/2019/0228/184642_f7a3024a_1554742.png)


#### 使用说明

1. 本地调试：scrapy crawl autohome_spider 
2. 放到linux环境下，执行./startup.sh 
3. myPymongodb.py //查询测试类，通过查询MongoDB各个车的口碑，算出平均分，生成雷达图。需要安装：pyecharts。
![image](https://images.gitee.com/uploads/images/2019/0228/184643_e5b17a42_1554742.png)
