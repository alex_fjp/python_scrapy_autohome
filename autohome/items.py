# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class AutohomeItem(scrapy.Item):
    # 汽车代码
    car_code = scrapy.Field()
    # 汽车名称
    car_name = scrapy.Field()
    # 购买车型: 凯美瑞, 2018款2.5HQ旗舰版
    car_type = scrapy.Field()
    # 购买地点: 哈尔滨
    car_buy_adr = scrapy.Field()
    # 购车经销商: & nbsp
    car_buy_merchant = scrapy.Field()
    # 购买时间: 2018年1月
    car_buy_date = scrapy.Field()
    # 裸车购买价: 27.98, 万元
    car_buy_price = scrapy.Field()
    # 油耗目前行驶: 6.4, 升 / 百公里, 20988, 公里
    car_fuel = scrapy.Field()
    # 耗电量目前行驶
    car_electric_driving = scrapy.Field()
    # 空间: 4
    car_score_space = scrapy.Field()
    # 动力: 5
    car_score_power = scrapy.Field()
    # 操控: 4
    car_score_driving = scrapy.Field()
    # 油耗: 5
    car_score_fuel = scrapy.Field()
    # 舒适性: 5
    car_score_comfort = scrapy.Field()
    # 外观: 4
    car_score_outward = scrapy.Field()
    # 内饰: 5
    car_score_interior = scrapy.Field()
    # 性价比: 4
    car_score_price = scrapy.Field()
    # 耗电量：5
    car_score_electric = scrapy.Field()
    # 购车目的: 上下班, 自驾游, 跑长途, 商务接送, 购物
    car_buy_purpose = scrapy.Field()
    # 平均分
    car_avg_score = scrapy.Field()

