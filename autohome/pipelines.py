# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import pymongo

class MongoAutohomePipeline(object):

    # 初始化mongodb数据的url、端口号、数据库名称
    def __init__(self, mongo_url, mongo_port, mongo_db):
        self.mongo_url = mongo_url
        self.mongo_port = mongo_port
        self.mongo_db = mongo_db

    # 读取settings里面的mongodb数据的url、port、DB。
    @classmethod
    def from_crawler(cls, crawlers):
        return cls(
            mongo_url=crawlers.settings.get('MONGO_URL'),
            mongo_port=crawlers.settings.get('MONGO_PORT'),
            mongo_db=crawlers.settings.get('MONGO_DB')
        )

    # 连接mongodb数据
    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_url, self.mongo_port)
        self.db = self.client[self.mongo_db]

    # 将数据写入数据库
    def process_item(self, item, spider):
        name = item.__class__.__name__ + '_koubei'
        self.db[name].insert(dict(item))
        return item

    # 关闭数据库
    def close_spider(self, spider):
        self.client.close()

class AutohomePipeline(object):
    def process_item(self, item, spider):
        return item
