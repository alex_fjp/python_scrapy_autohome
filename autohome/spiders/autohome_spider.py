# -*- coding: utf-8 -*-
# 抓取汽车之家的口碑

import scrapy
import logging
from autohome.items import AutohomeItem

'''
BeautifulSoup 是一个可以从HTML或XML文件中提取数据的Python库
按装：pip install beautifulsoup4
安装解析器：pip install lxml
'''
from bs4 import BeautifulSoup

base_url = 'https://k.autohome.com.cn/'

class AutohomeSpiderSpider(scrapy.Spider):
    name = 'autohome_spider'    # 爬虫名称 scrapy crawl autohome_spider
    allowed_domains = ['k.autohome.com.cn']
    # start_urls = [
    #     # 凯美瑞
    #     # 'https://k.autohome.com.cn/110/?pvareaid=2099118#dataList',
    #     # CRV
    #     # 'https://k.autohome.com.cn/314/#pvareaid=3454440',
    #     'https://k.autohome.com.cn/2664',
    # ]

    def start_requests(self):
        # 2664 特斯拉 Model X
        # 980 SAVANA
        # 110 凯美瑞
        # 314 CRV
        # 770 RAV荣放
        # 2987 马自达CX-5
        # 78 雅阁
        urls = ['110', '314', '770', '2987', '78']
        for url in urls:
            yield scrapy.Request(base_url + url, callback=self.parse, dont_filter=True)


    def parse(self, response):
        soup = BeautifulSoup(response.text, 'lxml')

        # 找到该车在汽车之家的代号
        link = soup.find('div', class_='subnav-title-name').find('a')
        car_code = link.get('href').split('/')[-2]
        car_name = link.get_text(strip=True)
        logging.info("汽车代号：%s", car_code)
        logging.info("汽车名称：%s", car_name)

        # 找到口碑左侧
        mou_lefts = soup.find_all('div', class_='mouthcon-cont-left')
        # 遍历每一条口碑
        for mou_left in mou_lefts:
            # 定义一个口碑内容对象
            autoitem = AutohomeItem()

            autoitem['car_code'] = car_code
            autoitem['car_name'] = car_name

            # 遍历所有口碑项
            dls = mou_left.find_all('dl', class_='choose-dl')
            for dl in dls:
                # print(dl.find('dt').get_text(strip=True) + ":", dl.find('dd').get_text(',', strip=True))
                # 评价项目
                dt_text = dl.find('dt').get_text(strip=True)
                # 内容
                dd_text = dl.find('dd').get_text(',', strip=True)
                self.set_autoitems(autoitem, dt_text, dd_text)

            # 计算这条口碑的平均分
            self.set_score_avg(autoitem)

            logging.info(autoitem)
            yield autoitem

        # 找到下一个链接，即翻页
        next_url = response.xpath("//a[@class='page-item-next']/@href").extract()
        logging.info("下一页：%s" % next_url)
        if next_url:
            yield scrapy.Request(base_url + next_url[0], callback=self.parse, dont_filter=True)


    '''
        给 items 赋值
    '''
    def set_autoitems(self, autoitem, dt_text, dd_text):
        if dt_text == '购买车型':
            autoitem['car_type'] = dd_text
        elif dt_text == '购买地点':
            autoitem['car_buy_adr'] = dd_text
        elif dt_text == '购车经销商':
            autoitem['car_buy_merchant'] = dd_text
        elif dt_text == '购买时间':
            autoitem['car_buy_date'] = dd_text
        elif dt_text == '裸车购买价':
            autoitem['car_buy_price'] = dd_text
        elif dt_text == '油耗目前行驶':
            autoitem['car_fuel'] = dd_text
        elif dt_text == '空间':
            autoitem['car_score_space'] = dd_text
        elif dt_text == '动力':
            autoitem['car_score_power'] = dd_text
        elif dt_text == '操控':
            autoitem['car_score_driving'] = dd_text
        elif dt_text == '油耗':
            autoitem['car_score_fuel'] = dd_text
        elif dt_text == '舒适性':
            autoitem['car_score_comfort'] = dd_text
        elif dt_text == '外观':
            autoitem['car_score_outward'] = dd_text
        elif dt_text == '内饰':
            autoitem['car_score_interior'] = dd_text
        elif dt_text == '性价比':
            autoitem['car_score_price'] = dd_text
        elif dt_text == '购车目的':
            autoitem['car_buy_purpose'] = dd_text
        elif dt_text == '耗电量目前行驶':
            autoitem['car_electric_driving'] = dd_text
        elif dt_text == '耗电量':
            autoitem['car_score_electric'] = dd_text
        else:
            logging.error("缺少类目[ %s : %s ]", dt_text, dd_text)

    '''
    将用户的所有打分加起来求平均分
    如果没有值默认 4分
    '''
    def set_score_avg(self, autoitem):
        items = dict(autoitem)
        space = int(items.get('car_score_space', 4))
        power = int(items.get('car_score_power', 4))
        drivi = int(items.get('car_score_driving', 4))
        sfuel = int(items.get('car_score_fuel', 4))
        comfo = int(items.get('car_score_comfort', 4))
        outwa = int(items.get('car_score_outward', 4))
        inter = int(items.get('car_score_interior', 4))
        price = int(items.get('car_score_price', 4))
        elect = int(items.get('car_score_electric', 4))

        sum_score = space + power + drivi + sfuel + comfo + outwa + inter + price + elect
        avg_score = sum_score / 9
        autoitem['car_avg_score'] = '%.1f' % avg_score

