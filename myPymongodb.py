from pymongo import MongoClient
from bson.objectid import ObjectId
from bson.son import SON
from pyecharts import Radar

# 连接
myclient = MongoClient('47.107.225.101', 27017)
# 指定数据库
mydb = myclient['autohome']
# 指定集合
mycol = mydb['AutohomeItem_koubei']

# 查询总条数
c = mycol.count_documents({})
print("总条数：", c)

# 查询一条数据
x = mycol.find_one()
print("第一条：", x)
'''

# 带条件查询 需要导入包：from bson.objectid import ObjectId
x = mycol.find_one({'_id': ObjectId('5c654e954d45074d8228b0b6')})
print("根据_id查询：", x)

# 查询指定条数
kbs = mycol.find().limit(5)
for i, kb in enumerate(kbs, 1):
    print("第 %s 条：" % i, kb)



# 每个车的总的平均得分
pipeline = [
    {'$group': {'_id': '$car_name', 'avgnum': {'$avg': '$car_avg_score'}, 'count': {'$sum': 1}}},
    {"$sort": SON([('avgnum', 1)])}
]
scores = mycol.aggregate(pipeline)
for s in scores:
    print(s)
    print(s.get('_id'), '总评价条数：%s' % s.get('count'), '得分：%.2f' % s.get('avgnum'))
'''

# 计算每个车单项平均得分，生成雷达图
pipeline = [
    #{'$match':{'car_code': '110'}},
    {'$group': {
        '_id': {'car_name': '$car_name', 'car_code': '$car_code'},
        'avg_score_space': {'$avg': '$car_score_space'},
        'avg_score_power': {'$avg': '$car_score_power'},
        'avg_score_driving': {'$avg': '$car_score_driving'},
        'avg_score_fuel': {'$avg': '$car_score_fuel'},
        'avg_score_comfort': {'$avg': '$car_score_comfort'},
        'avg_score_outward': {'$avg': '$car_score_outward'},
        'avg_score_interior': {'$avg': '$car_score_interior'},
        'avg_score_price': {'$avg': '$car_score_price'},
        'avg_score_electric': {'$avg': '$car_score_electric'},
        'avg_count': {'$avg': '$car_avg_score'},
        'count': {'$sum': 1}
    }}
]


schema = [
    ("空间", 5), ("动力", 5), ("操控", 5), ("油耗", 5), ("舒适性", 5), ("外观", 5), ("内饰", 5), ("性价比", 5), ("总得分", 5)
]
range_color = ["#6a6b5a", "#6c27e3", "#83f9f1", "#30913b", "#da5a44", "#36552b"]
radar = Radar()
radar.config(schema)
scores = mycol.aggregate(pipeline)
for i, s in enumerate(scores):
    print(i, s.get('_id').get('car_name'), s.get('_id').get('car_code'), range_color[i])
    msg = '空间：%s，动力：%s，操控：%s，油耗：%s，舒适性：%s，外观：%s，内饰：%s，性价比：%s，总得分：%s' % \
          ('%.2f' % s.get('avg_score_space'),
           '%.2f' % s.get('avg_score_power'),
           '%.2f' % s.get('avg_score_driving'),
           '%.2f' % s.get('avg_score_fuel'),
           '%.2f' % s.get('avg_score_comfort'),
           '%.2f' % s.get('avg_score_outward'),
           '%.2f' % s.get('avg_score_interior'),
           '%.2f' % s.get('avg_score_price'),
           '%.2f' % s.get('avg_count'))
    # print(msg)
    v1 = [[
        float('%.2f' % s.get('avg_score_space')),
         float('%.2f' % s.get('avg_score_power')),
         float('%.2f' % s.get('avg_score_driving')),
         float('%.2f' % s.get('avg_score_fuel')),
         float('%.2f' % s.get('avg_score_comfort')),
         float('%.2f' % s.get('avg_score_outward')),
         float('%.2f' % s.get('avg_score_interior')),
         float('%.2f' % s.get('avg_score_price')),
         float('%.2f' % s.get('avg_count'))
    ]]
    print(v1)
    radar.add(s.get('_id').get('car_name'), v1, item_color=range_color[i], is_area_show=True, is_splitline=True, is_axisline_show=False)

radar.width = 1280
radar.height = 800
radar.radar_text_size = 40
radar.render()

